# README #

This is for X2, X3 monitoring boxes based upon the installs on Lonsdale site.
They differ to the X4 ones due to their use of Modbus sensors, an expansion module, and possibly PLC.

### What is this repository for? ###

* Monitoring performance of a single CW-X2 or CW-X3 via direct sensors and Modbus TCP connection to the CW-X PLC.

### Who do I talk to? ###

* Jack Arney (Developer)
* Denny Hagias (Project Manager)
